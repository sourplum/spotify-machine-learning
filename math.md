## Math

Causal reasoning and diagnostics:

Causal reasoning as a way to make a diagnosis seems convincing

https://link.springer.com/article/10.1007%2FBF01313345

---

Reinforcement learning:

Reinforcement learning (RL) is an area of machine learning concerned with how software agents ought to take actions in an environment so as to maximize some notion of cumulative reward.

![The typical framing of a Reinforcement Learning](./img/Reinforcement_learning_diagram.svg)

---

Collaborative filtering

Collaborative filtering (CF) is a technique used by recommender systems.

https://en.wikipedia.org/wiki/Collaborative_filtering

![Example of predicting of the user's rating using collaborative filtering](./img/Collaborative_filtering.gif)

---

https://dl.acm.org/citation.cfm?doid=3240323.3240354

---

Concours Kaggle

https://kaggle.com

---

Pareto principle

---

System with objectives:
+ diversity
+ serendipity
+ novelty
+ coverage

---

## *le* CircleJerk / { E C H O    C H A M B E R S }

The quantity of interest (estimand) is no longer the rating of an item, but the probability of the user selecting an item

["Quote: estimand"]("./1710.11214.pdf")

---

[Arxiv Publication research]("https://arxiv.org/search/?query=spotify&searchtype=all")

---

The ACM Recommender Systems Challenge 2018 focused on the task of automatic music playlist continuation, which is a form of the more general task of sequential recommendation. Given a playlist of arbitrary length with some additional meta-data, the task was to recommend up to 500 tracks that fit the target characteristics of the original playlist. For the RecSys Challenge, Spotify released a dataset of one million user-generated playlists. Participants could compete in two tracks, i.e., main and creative tracks. Participants in the main track were only allowed to use the provided training set, however, in the creative track, the use of external public sources was permitted. In total, 113 teams submitted 1,228 runs to the main track; 33 teams submitted 239 runs to the creative track. The highest performing team in the main track achieved an R-precision of 0.2241, an NDCG of 0.3946, and an average number of recommended songs clicks of 1.784. In the creative track, an R-precision of 0.2233, an NDCG of 0.3939, and a click rate of 1.785 was obtained by the best team. This article provides an overview of the challenge, including motivation, task definition, dataset description, and evaluation. We further report and analyze the results obtained by the top performing teams in each track and explore the approaches taken by the winners. We finally summarize our key findings and list the open avenues and possible future directions in the area of automatic playlist continuation.

Text from [@DBLP:journals/corr/abs-1810-01520].

---

[Use markdown and bibtex](https://gist.github.com/maxogden/97190db73ac19fc6c1d9beee1a6e4fc8)

[Exemple markdown and bibtex](https://github.com/datproject/docs/tree/master/papers)

---

http://www.recsyschallenge.com/2018/

https://recsys-challenge.spotify.com/

https://papers.nips.cc/paper/5004-deep-content-based-music-recommendation
