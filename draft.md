---
title: "Recommendation System with Spotify"
date: "Dec 2018 (last updated: Jan 2019)"
author: "Cedric Bonhomme, Maxime Cohen"
header-includes:
    - \usepackage{multicol}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---

# Abstract

Recommender Systems (RS) are software applications that support users in finding items of interest within larger collections of objects, often in a personalized way.
Spotify a music on demand streaming service, use recommendation system algorithms to persuade its users to explore artists songs on the platform or listen to a particular song.
As [@2018arXiv181211740L]'s approach on recommendation system states out, many models focus on minimizing the error of rating prediction, but fails to give more importance on the explanation behind each recommendations.
This paper will state problems behind recommendation systems for music on demand service and present Spotify approach to the problem with a focus on explanations on recommender system "recsplanations" [@McInerney:2018:EEE:3240323.3240354]

# 0. Background

Recommendation systems have its roots in machine learning (cf. “rating prediction”) and information retrieval (cf. “retrieving” items based on implicit “queries” given by user preferences). [@2017arXiv171003208S]
Over the year and with the apparition of 3 main platforms, there has been a huge demand of recommendation system for e-commerce platforms, video-on-demand streaming platform and in our case study music-on-demand streaming platform in order to build customer loyalty.
Spotify is a leader across most streaming-service markets [^1] and the recommendations produced by the service result from several years of testing.

[^1]: http://www.midiaresearch.com/downloads/midia-streaming-music-metrics-bundle/

# 1. Music recommendation

Recommendation for the user is quite common for:

+ e-commerce platforms to suggest an item to add to the basket, home page with deals on items known to interest the user
+ video on demand platforms where features will be extracted from video flux, tags, to be able to recommend the next movie/series to watch
+ music on demand platform, this time with songs and playlist as input items
+ personalised product advertisement, as done by Google, Facebook and Apple where the user profile will be trained with collected data from user interactions with the system in order to market a product to the correct demographic

On music platforms, the recommendation system has to handle the following aspects.
From [@2017arXiv171003208S] paper, the author list the particularities of the music recommendation.

+ Duration of items:
	+ Movie: `>` 90min
	+ Book: `>` 1day
	+ Music: `>` 3min && `<` 5min or more for classical music
+ Magnitude of items:
	+ The size of music catalogs tends to be much more important when dealing with music, however video catalogs are now also having the same problem
+ Sequential consumption:
	+ Movie: The user watch one movie at the time
	+ Music: The user has to listen to a playlist composed of multiple songs, a certain harmony between songs should be present (eg. A Jazz playlist should not contain Hard Metal songs). However recent research has found that the exact order of songs does not actually matters to the users [@Tintarev:2017:SDS:3079628.3079633], song-to-song transitions [@Kamehkhosh2018HowAR] and the ensemble of songs in a playlist [@Vall2017TheIO] do matter.
+ Recommendation of previously recommended items:
	+ It is acceptable for a song to be suggested multiple times. The user wants to hear new songs but an old and familiar one can be suggested again. On video platforms a movie already watched will not be recommended twice.
+ Consumption behavior:
    + Getting feedbacks from the user can be tricky as while a playlist is playing, the user will fail to give its taste on each song (playlist are often listened passively while doing another task). Also the duration of a song is small compared to a movie where the user will be more inclined to give a grade or even a review.
+ Listening context:
    Location and time of the day are important factor when recommending playlists.
    On the road, at work or at home? Day or Night?

# 1.1 Evaluating recommender algorithms

When training the model it is important to be able to evaluate the recommender algorithms.
The agent needs to be rewarded, as will be seen in Figure 1.
This evaluation will allow to edit the weigh of each node until the recommendation are satisfying to the users.

The following metrics are the most commonly used for recommendation system.

---

*Mean absolute error (MAE)* is one the most common metrics for evaluation the prediction of recommender algorithms.
It computes the average absolute deviation, between the *predicted ratings* and the *actual ratings* as provided by users [@Herlocker:2004:ECF:963770.963772].

$MAE=\frac{1}{|T|}\displaystyle\sum_{r_{u,i} \in T} |r_{u,i} - \hat{r}_{u,i}|$

$r_{u,i}$ and $\hat{r}_{u,i}$ the actual and the predicted ratings of item i for the user u.

---

*Mean average at top K recommendations (MAP@K)* is a *rank-based* metric. It computes the *overall precision* of the system, at *different lengths* of *recommendation lists* [@Herlocker:2004:ECF:963770.963772].

$AP@K = \frac{1}{N}\displaystyle\sum_{i=1}^{K} P@i \times rel(i)$

With *rel(i)* indicating if the $i^{th}$ recommended item is relevant (1 or 0).

*N* the total number of relevant items and *K* the size of the recommendation list.

---

Another variant, made popular by the Kaggle[^2] competitions is:

[^2]: https://www.kaggle.com/c/avito-prohibited-content/discussion

$AP@K = \frac{1}{min(K, N)} \displaystyle\sum_{i=1}^{N} P@i \times rel(K)$

With *N* the total number of relevant items and *K* the size of the recommendation list.

# 2. Bandit

Bandits introduce exploration as a way of encouraging items with high uncertainty to be explored (indie musicians and songs).

The *Multi-Armed Bandit* (MAB) problem is:
A resource allocation problem concerned with allocating one or more resources among several alternative (competing) projects.
A conflict arise between making decisions (allocating resources) that yield high current rewards, versus making decisions that sacrifice current gains with the prospect of better future rewards [@Mahajan_multi-armedbandit].

While important for decision making, MAB has however no room for learning the structure and generalization to new decision situations [@stojic_p.analytis_speekenbrink_2015].
Existing work has looked at bandits and explanations independently. Spotify provide the first method that combine both.

## 2.1 Bart

Bart [@McInerney:2018:EEE:3240323.3240354] is the framework developed by Spotify to balance exploration with exploitation in recommendation.
The purpose of Bart is to learn how items and explanations interact
within any given context to predict user satisfaction [@McInerney:2018:EEE:3240323.3240354].

Bart is composed of [@McInerney:2018:EEE:3240323.3240354]:

+ user preference model
	+ Factorization machine: contextual features about user, item, explanations to predict user engagement
+ ranking strategy
	+ Epsilon-greedy to randomly select the shelf and randomly select the item within each shelf.
+ training procedure
	+ Undo the bias introduced by the recommender engine

## 2.2 Reward model

The reward model should predict the user response given a context and recommended item [@McInerney:2018:EEE:3240323.3240354].

The predicted reward is computed as:

$\textit{predicted reward} = \mathbb{E}_{p_{\theta}(R|A,X)}[R]$

+ *X* the context
+ *A* the recommended item
+ *R* the user response (0 or 1 if the user play the recommended song or not)
+ *$\theta$* a set of model parameters

## 2.3 Stochastic policy / Random process

To balances exploitation (choosing high reward actions) and exploration (finding high reward actions): upper confidence bound, ,T hompson sampling or Epsilon greedy can be used.

Epsilon greedy (strategy for approximate solution of bandit problem) algorithm takes the best action most of the time, but does random exploration occasionally.
Epsilon greedy algorithm is as follow:

$\pi(A|X) = (1 - \epsilon) + \frac{\epsilon}{|\mathcal{A}|}$ when $A = A^{*}$

$\pi(A|X) = \frac{\epsilon}{|\mathcal{A}|}$ otherwise

# 3. Concepts

## 3.1 Causal reasoning

Causal reasoning [@Rizzi1994] is a way to make diagnosis seems convincing.

## 3.2 Collaborative filtering algorithms

Collaborative filtering [@2018arXiv181211740L] is a technique commonly used by recommender systems. It is based on the tastes of the closest nodes (friends) to make assumption on the user's tastes.

However this approach suffers from the cold start problem: it fails when no usage data is available [@NIPS2013_5004]. If no one listen to a music it will then never be recommended. Thus popular songs stay popular, and new and unpopular songs cannot be recommended.

Example of predicting of the user's rating using collaborative filtering:

\Begin{multicols}{2}

![Example of predicting of the user's rating using collaborative filtering](./img/collaborative_filtering/06.png)
![Example of predicting of the user's rating using collaborative filtering](./img/collaborative_filtering/15.png)
![Example of predicting of the user's rating using collaborative filtering](./img/collaborative_filtering/14.png)
![Example of predicting of the user's rating using collaborative filtering](./img/collaborative_filtering/16.png)

\End{multicols}

Based on the evaluation made by the nearest peers (friends of user). A recommendation has been made that follow the overall tendency the user would follow if part of the same community. While the easiest to implement, this approach on it's own fail to detect if a user has different taste in movie than its friends.

## 3.3 Content based recommendation

Content-based recommendation (CB) algorithms do not require ratings of users other than the target user. Therefore, as long as some pieces of information about the user’s own preferences are available, such techniques can be used in cold start scenarios
Content-based recommendation algorithms analyse the content of the items in question, here a chunk of a song, and extract a number of features that define the acoustic properties of the audio signal. [@2017arXiv171003208S]

Informations such as the perceived mood, music genre, ...
This algorithms fails to guess certain information like the geographical location or the popularity of a song.

Features can also be extracted manually with the help of users (select a tag), as done by the [Pandora’s Music Genome Project](htp://www.pandora.com/about/mgp).

## 3.4 Reinforcement learning

Reinforcement learning is an area of machine learning concerned with how software agents ought to take actions in an environment so as to maximize some notion of cumulative reward.

![The typical framing of a Reinforcement Learning](./img/Reinforcement_learning_diagram.png){width=50%}

## 3.5 Automatic Playlist Continuation

A playlist can be defined as a sequence of tracks intended to be played together [@2017arXiv171003208S].
Authors have proposed approaches on *Markov chains*.
A Markov chain respect the *Markov property* that states that the future of the process is conditionally independent of the past, given the present. Or that the “memory” in the process lasts only one time step [@Mahajan_multi-armedbandit].

In [@McFee2011TheNL] the authors use Markov chain *mixtures*, as it is a typical *list continuation* problem. With mixtures trained on a dataset of playlists[^3].

[^3]: [Art Of The Mix](http://www.artofthemix.org/) is a playlist catalog with user generated playlists, it can be exported as a dataset.
[Million Song Dataset](https://labrosa.ee.columbia.edu/millionsong/pages/getting-dataset) is a huge dataset (300GiB) used by researchers to train models that scales to commercial sizes.

However one of the drawbacks of Markov chains is that it assumes that: "the next user action" **depend only on** a **limited number of** "the most recent actions" [@Quadrana2018SequenceAwareRS].

[@McFee2011TheNL] suggest an evaluation procedure for playlist generation using techniques from natural language processing.
[@Quadrana2018SequenceAwareRS] suggest a sequence-aware recommender system with the following image as the overview.

![High-level Overview of Sequence-Aware Recommendation Problems](./img/overview_sequence_aware_recommendation_problem.png)

## 3.5.1 Playlist evaluation

Playlist evaluation can be grouped into three categories [@Quadrana2018SequenceAwareRS]:

+ human evaluation: tests subjects evaluate the resulting playlists on a scale of 1 to 10. Those studios can then provide evidence that one algorithms outperforms another, however as any human evaluation the human factor make it hard to reproduce.
+ semantic cohesion: It evaluate the cohesion of a generated playlist. The cohesion is the frequency counts of meta-data co-occurance, as an example the frequency count of songs by the same artist.
+ sequence prediction: prediction on the ranking of a song.

# 3.6 Evaluation measures

Evaluation measures of type beyond-accuracy do not need ranking and so do not suffer from cold-start problem.
The only problem is that those measures use factors hard to describe mathematically
[@2017arXiv171003208S].

## Spread

Entropy of the distribution of the items recommended to the users.

## Coverage

Proportion of items over which the system is capable of generating recommendations.

## Novelty

Ability to recommend new items that the user did not know about before.

## Serendipity

Evaluation based on relevant and surprising recommendation.

## Diversity

Extant to which recommended items are different from each other.

# Acknowledgements

This work was made possible thanks to the teaching of Ikram Chraibi Kaadoud on neural networks.

# References
