## Dependencies

```bash
pacman -S pandoc pandoc-citeproc texlive-core
```

## Build

```bash
./build.sh
```

Will generate `draft.pdf` portable document file.

---

http://engineeringjobs4u.co.uk/spotify-ml-day-july-9th-2018-stockholm

http://jamesmc.com/

http://jamesmc.com/blog/2018/10/1/explore-exploit-explain
[doc]("http://jamesmc.com/s/BartRecSys.pdf")
[doc]("http://jamesmc.com/s/bart_recsys.pdf")

```markdown
James McInerney, Spotify
Explore, Exploit, and Explain: Personalizing Explainable Recommendations with Bandits

 

The multi-armed bandit is an important framework for balancing exploration with exploitation in recommendation. In parallel, explaining recommendations (“recsplanations”) is crucial if users are to understand their recommendations. We provide the first method that combines exploration and explanations in a principled manner. In particular, our method is able to jointly (1) learn which explanations each user responds to; (2) learn the best content to recommend for each user; and (3) balance exploration with exploitation to deal with uncertainty. Experiments with historical log data and tests with live production traffic in a large-scale music recommendation service show a significant improvement in user engagement.

Presenter Bio:

James is a Senior Research Scientist at Spotify, New York. He works on scalable probabilistic methods and recommendation. He was previously a postdoc at Columbia University and Princeton University and obtained a PhD from the University of Southampton on the topic of Bayesian models for spatio-temporal data.
```

https://github.com/rabitt?tab=repositories

```markdown
Rachel Bittner, Spotify
Multitask Learning for Fundamental Frequency Estimation in Music

 

Fundamental frequency (f0) estimation from polyphonic music includes the tasks of multiple-f0, melody, vocal, and bass line estimation. Historically these problems have been approached separately and until recently little work had used learning-based approaches. We present a multi-task deep learning architecture that jointly predicts outputs for multi-f0, melody, vocal and bass line estimation and is trained using a large, semi-automatically annotated dataset. We provide evidence of the usefulness of the recently proposed Harmonic CQT to fundamental frequency. Finally, we show that the multitask model outperforms its single-task counterparts, and that the addition of synthetically generated training data is beneficial.


Presenter Bio:

Rachel is a Research Scientist at Spotify in New York City, and recently completed her Ph.D. at the Music and Audio Research Lab at New York University under Dr. Juan P. Bello. Her research interests are at the intersection of audio signal processing and machine learning, applied to musical audio. Her dissertation work applied machine learning to various types of fundamental frequency estimation.
```

