#!/usr/bin/env sh

pandoc --filter pandoc-citeproc --bibliography=draft.bib --variable classoption=onecolumn --variable papersize=a4paper -s draft.md -o bonhomme_cohen_recommendation_systems.pdf
